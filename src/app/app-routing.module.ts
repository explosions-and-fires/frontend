import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { AuthGuard } from './auth.guard';
import { AdminGuard } from './admin.guard';
import { LoginComponent } from './components/login/login.component';
import { FiresComponent } from './components/fires/fires.component';
import { ChemicalComponent } from './components/chemical/chemical.component';
import { ExplosionsComponent } from './components/explosions/explosions.component';
import { CalculationsComponent } from './components/calculations/calculations.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';

const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: MainComponent, canActivate: [AuthGuard], data: {name: 'ExFire'}},
  {path: 'calculations', redirectTo: 'main'},
  {
    path: 'calculations', component: CalculationsComponent, canActivate: [AuthGuard], children: [
      {path: 'fires', component: FiresComponent, canActivate: [AuthGuard], data: {name: 'Последствия лесных пожаров'}},
      {path: 'chemical', component: ChemicalComponent, canActivate: [AuthGuard], data: {name: 'Последствия заражения СДЯВ'}},
      {path: 'explosions', component: ExplosionsComponent, canActivate: [AuthGuard], data: {name: 'Последствия взрывов ТВС'}},
    ]
  },
  {path: 'admin', component: AdminPanelComponent, canActivate: [AdminGuard], data: {name: 'Панель управления'}},
  {path: 'login', component: LoginComponent, data: {name: 'ExFire'}},
  {path: '**', redirectTo: 'main'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
