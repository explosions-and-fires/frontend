import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MapComponent } from './components/map/map.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { ChemicalComponent } from './components/chemical/chemical.component';
import { FiresComponent } from './components/fires/fires.component';
import { ExplosionsComponent } from './components/explosions/explosions.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorsInterceptor } from './interceptors/errors.interceptor';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { LoadingComponent } from './components/helpers/loading/loading.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ToastrModule } from 'ngx-toastr';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDividerModule } from '@angular/material/divider';
import { LoginService } from './components/login/login.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { FirstUppercasePipe } from '../pipes/first-uppercase.pipe';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CalculationsComponent } from './components/calculations/calculations.component';
import { FormatInterceptor } from './interceptors/format.interceptor';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    NavbarComponent,
    LoginComponent,
    MainComponent,
    ChemicalComponent,
    FiresComponent,
    ExplosionsComponent,
    LoadingComponent,
    FirstUppercasePipe,
    CalculationsComponent,
    AdminPanelComponent,
  ],
  exports: [
    MatButtonModule,
    MatInputModule,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatCheckboxModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MatChipsModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatExpansionModule,
    MatTooltipModule,
    MatCardModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorsInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: FormatInterceptor, multi: true},
    LoginService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
