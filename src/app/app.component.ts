import {Component, DoCheck} from '@angular/core';
import {AuthGuard} from './auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'ExFire';

  constructor() {
  }
}
