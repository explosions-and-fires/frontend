import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class FormatInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.includes('/response')) {
      return next.handle(request).pipe(
        map((res: HttpResponse<any>) => {
          if (res instanceof HttpResponse) {
            res = res.clone({body: this.modifyBody(res.body)});
          }
          return res;
        }));
    } else {
      return next.handle(request);
    }
  }

  private modifyBody(body: any): any {
    const formatNumber = (bodyObj: any) => {
      for (const item of Object.keys(bodyObj)) {
        let value = bodyObj[item];
        if (value !== undefined || null) {
          if (typeof value === 'number') {
            bodyObj[item] = parseFloat(value.toFixed(3));
          } else if (typeof value === 'object') {
            value = formatNumber(value);
          } else if (Array.isArray(value)) {
            for (const i of value) {
              value.push(formatNumber(value[i]));
            }
          } else {
            bodyObj[item] = value;
          }
        }
      }
    };
    formatNumber(body);
    return body;
  }
}
