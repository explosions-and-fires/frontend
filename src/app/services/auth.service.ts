import { Injectable } from '@angular/core';
import { InformerService } from './informer.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private informer: InformerService,
    private router: Router
  ) {
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  checkIfTokenValid(): void {
    this.http.get(`${environment.basicURL}/echo`, {
      observe: 'response'
    }).toPromise().then((res) => {
      const status = res.status;
      if (status !== 204) {
        this.logout();
        this.informer.ErrorMessage('Время вашей сессии истекло, пожалуйста, войдите повторно');
      }
    });
  }
}
