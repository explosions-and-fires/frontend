import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxLanguage from '@mapbox/mapbox-gl-language';
import * as turf from '@turf/turf';
import { AnswersResponse } from '../components/explosions/explosions.service';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import * as GeoJSON from 'geojson';
import { InformerService } from './informer.service';

export interface LatLon {
  lat: number;
  lon: number;
}

@Injectable({
  providedIn: 'root'
})
export class MapService {
  map: mapboxgl.Map;
  newStyle: string;
  zoom = 10;
  lat = 55.751244;
  lng = 37.618423;
  marker = new mapboxgl.Marker({
    draggable: true,
  });
  markerRed = new mapboxgl.Marker({
    draggable: true,
    color: 'red'
  });
  draw = new MapboxDraw({
    displayControlsDefault: false,
    controls: {
      polygon: true,
      trash: true
    }
  });
  geocoder = new MapboxGeocoder({
    accessToken: environment.mapbox.accessToken,
    mapboxgl,
    marker: false,
    countries: 'ru',
    placeholder: 'Найти объект на карте'
  });
  cLng: number;
  cLat: number;
  clearFires: boolean;
  distance: number;
  to = [this.lng, this.lat];
  from = [this.lng, this.lat];
  roundedArea: number;
  perimeter: number;
  markerCallBack = () => {
  }

  constructor(private informer: InformerService) {
    this.marker.getElement().addEventListener('click', () => {
    });
    this.distance = 0.1;
  }

  switchLayer(layer): void {
    this.map.setStyle('mapbox://styles/mapbox/' + layer);
  }

  buildMap(): mapboxgl.Map {
    this.map = new mapboxgl.Map({
      accessToken: environment.mapbox.accessToken,
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: this.zoom,
      doubleClickZoom: false,
      center: [this.lng, this.lat]
    });
    this.map.once('load', () => {
      this.map.resize();
    });
    this.map.doubleClickZoom.disable();
    this.switchLanguage();
    this.map.addControl(this.geocoder, 'top-left');
    this.setMarkerOnGeocoderResult();
    this.setMarkerOnDblClick();
    this.getDistance();
    this.drawPolygon();
    return this.map;
  }

  resetMap(completeReset: boolean): void {
    this.removeSector();
    this.removePolygon();
    this.removeCircles();
    this.draw.deleteAll();
    this.draw.trash();
    this.clearFires = true;
    if (completeReset) {
      this.removeMarkerV2();
      this.removeSecMarkerV2();
      this.distance = 0.1;
    }
    this.flyToMarker();
  }

  flyToMarker(): void {
    const lngLat = this.marker.getLngLat();
    this.map.flyTo({center: lngLat, zoom: 9});
  }

  switchLanguage(): void {
    const mapboxLanguage = new MapboxLanguage({
      defaultLanguage: 'ru'
    });
    this.map.addControl(mapboxLanguage);
  }

  removeMarker(): void {
    this.marker.remove();
  }

  createCircles(radiuses: AnswersResponse['damageRadiuses']): void {
    this.removeCircles();
    const descriptions = [];
    const deltaP = [];
    const radius = [];
    const colors = ['#ff0000', '#ff5100', '#fff400', '#00aeff', '#26ff00'];
    const lngLat = this.marker.getLngLat();
    const center = [lngLat.lng, lngLat.lat];
    const circles = [];
    const hollowCircles = [];
    for (const item of radiuses) {
      circles.push(turf.circle(center, item.radius.value, {steps: 64, units: 'meters'}));
      descriptions.push(item.characteristic);
      radius.push(item.radius.value);
      deltaP.push(item.deltaP);
    }
    hollowCircles.push(circles[0]);
    for (let i = 0; i < circles.length; i++) {
      hollowCircles.push(turf.mask(circles[i], circles[i + 1]));
    }
    this.map.once('zoom', () => {
      for (let i = 0; i < circles.length; i++) {
        this.map.addSource(`circle${i}`, {
          type: 'geojson',
          data: hollowCircles[i]
        });
        this.map.addLayer({
          id: `circle${i}`,
          type: 'fill',
          source: `circle${i}`,
          paint: {
            'fill-color': colors[i],
            'fill-opacity': 0.5
          }
        });
        this.map.addLayer({
          id: `outlineCircle${i}`,
          type: 'line',
          layout: {},
          source: `circle${i}`,
          paint: {
            'line-color': '#000',
            'line-width': 1
          }
        });
      }
    });
    for (let i = 0; i < descriptions.length; i++) {
      this.map.on('click', `circle${i}`, (e) => {
        new mapboxgl.Popup()
          .setLngLat(e.lngLat)
          .setHTML(`<p>${descriptions[i]}</p><p>${deltaP[i].description}: ${deltaP[i].value} Па.</p>
<p>Радиус: ${radius[i]} м.</p>`)
          .addTo(this.map);
      });
      this.map.on('mousemove', `circle${i}`, (e) => {
        this.map.setPaintProperty(`circle${i}`, 'fill-opacity', 0.65);
        this.map.getCanvas().style.cursor = 'pointer';
      });
      this.map.on('mouseleave', `circle${i}`, () => {
        this.map.setPaintProperty(`circle${i}`, 'fill-opacity', 0.5);
        this.map.getCanvas().style.cursor = '';
      });
    }
    const [x1, y1, x2, y2] = turf.bbox(hollowCircles[4]);
    this.map.fitBounds([x1, y1, x2, y2], {animate: true, padding: 60});
    this.informer.ShowInformation('Для дополнительной информации кликните по кругу или маркеру');
  }

  removeCircles(): void {
    for (let i = 0; i < 5; i++) {
      if (this.map.getLayer(`outlineCircle${i}`)) {
        this.map.removeLayer(`outlineCircle${i}`);
      }
      if (this.map.getLayer(`circle${i}`)) {
        this.map.removeLayer(`circle${i}`);
      }
      if (this.map.getSource(`circle${i}`)) {
        this.map.removeSource(`circle${i}`);
      }
    }
  }

  setRedMarkerPopup(probits: AnswersResponse['probits']): void {
    let counter = 0;
    const html = document.createElement('mat-accordion');
    // const headers = ['Повреждение стен', 'Разрушение промышленных зданий', 'Повреждение стен при которых возможно...',
    //   'Разрыв барабанных перепонок', 'Отброс людей волной'];
    for (const item of probits) {
      const expPanel = document.createElement('mat-expansion-panel');
      expPanel.setAttribute('hideToggle', '');
      const p = document.createElement('p');
      const descNode = document.createTextNode(`${item.description}: ${item.value}%`);
      p.appendChild(descNode);
      expPanel.appendChild(p);
      html.appendChild(expPanel);
      counter++;
    }
    const popup = new mapboxgl.Popup().setHTML(`${html.outerHTML}`);
    this.markerRed.setPopup(popup);
  }


  createSector(radius: number, angle: number, windAngle: number): void {
    this.removeSector();
    const markerLngLat = this.marker.getLngLat();
    // tslint:disable-next-line:no-shadowed-variable
    const center = turf.point([markerLngLat.lng, markerLngLat.lat]);
    const bearing1 = windAngle - (angle / 2);
    const bearing2 = windAngle + (angle / 2);
    const sector = turf.sector(center, radius, bearing1, bearing2);

    this.map.once('zoom', (e) => {
      this.map.addSource('sector', {
        type: 'geojson',
        data: sector
      });
      this.map.addLayer({
        id: 'sector',
        type: 'fill',
        source: 'sector',
        layout: {},
        paint: {
          'fill-color': '#ff0000',
          'fill-opacity': 0.25,
          'fill-outline-color': '#4e0000'
        }
      });
    });
    const [x1, y1, x2, y2] = turf.bbox(sector);
    this.map.fitBounds([x1, y1, x2, y2], {animate: true, padding: 60});
  }

  removeSector(): void {
    if (this.map.getLayer('sector')) {
      this.map.removeLayer('sector');
    }
    if (this.map.getSource('sector')) {
      this.map.removeSource('sector');
    }
  }

  drawPolygon(): void {
    this.map.addControl(this.draw);
    const updateArea = (e) => {
      const data = this.draw.getAll();
      if (data.features.length > 0) {
        const area = (turf.area(data) / 10000);
        const perimeter = turf.length(data, {units: 'meters'});
        this.roundedArea = parseFloat(area.toFixed(3));
        this.perimeter = parseFloat(perimeter.toFixed(3));
      }
    };
    this.map.on('draw.create', updateArea);
    this.map.on('draw.delete', updateArea);
    this.map.on('draw.update', updateArea);
    this.map.on('draw.modechange', (e) => {
      const data = this.draw.getAll();
      if (this.draw.getMode() === 'draw_polygon') {
        const pids = [];
        const lid = data.features[data.features.length - 1].id;
        data.features.forEach((f) => {
          if (f.geometry.type === 'Polygon' && f.id !== lid) {
            pids.push(f.id);
          }
        });
        this.draw.delete(pids);
      }
    });
  }

  drawFinalPolygon(rotateAngle: number, area: number): void {
    this.removePolygon();
    const areaValue = area;
    const steps = 200;
    let counter = 0;
    const data = this.draw.getAll();
    // const north = turf.point([59.1422, 81.5035]);
    // const bearing = turf.bearing(north, data.features[0].geometry.coordinates[0][0]);
    // const angleToNorth = turf.bearingToAngle(bearing);
    const areaPolygon = (areaValue / (turf.area(data) / 10000));
    const scaledPolygon = turf.transformScale(data, areaPolygon);
    const rotatedPolygon = turf.transformRotate(scaledPolygon, rotateAngle);
    const smallPolygonPoints = data.features[0].geometry.coordinates[0];
    const bigPolygonPoints = rotatedPolygon.features[0].geometry.coordinates[0];
    const routes = [];
    for (let i = 0; i < smallPolygonPoints.length - 1; i++) {
      const route = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'LineString',
              coordinates: null
            }
          }
        ]
      };
      const line = [];
      const linestring = turf.lineString([smallPolygonPoints[i], bigPolygonPoints[i]], {name: `line${i}`});
      const lineDistance = turf.length(linestring, {units: 'meters'});
      for (let j = 0; j < lineDistance; j += lineDistance / steps) {
        const segment = turf.along(linestring, j, {units: 'meters'});
        line.push(segment.geometry.coordinates);
      }
      route.features[0].geometry.coordinates = line;
      routes.push(route);
    }
    this.map.once('render', () => {
      this.map.addSource('finalPolygon', {
        type: 'geojson',
        data: null
      });
      this.map.addLayer({
        id: 'finalPolygon',
        type: 'fill',
        source: 'finalPolygon',
        layout: {},
        paint: {
          'fill-color': '#ff7600',
          'fill-opacity': 0.5
        }
      });
      this.map.addLayer({
        id: 'outline',
        type: 'line',
        source: 'finalPolygon',
        layout: {},
        paint: {
          'line-color': '#ff1500',
          'line-width': 20,
          'line-blur': 20,
          'line-opacity': .4
        }
      });
      animate();
    });
    const animate = () => {
      const points = [];
      // tslint:disable-next-line:prefer-for-of
      for (let j = 0; j < routes.length; j++) {
        const start = routes[j].features[0].geometry.coordinates[counter >= steps ? counter - 1 : counter];
        const end = routes[j].features[0].geometry.coordinates[counter >= steps ? counter : counter + 1];
        if (!start || !end) {
          return;
        }
        const updatePoint = pointOnLine(routes[j].features[0].geometry.coordinates[counter]);
        points.push(updatePoint);
      }
      const newPoints = turf.featureCollection(points);
      const finalPolygon = turf.convex(newPoints);
      const smoothPolygon = turf.polygonSmooth(finalPolygon, {iterations: 3});
      const polygon = this.map.getSource('finalPolygon') as mapboxgl.GeoJSONSource;
      polygon?.setData(smoothPolygon);
      const [x1, y1, x2, y2] = turf.bbox(smoothPolygon);
      this.map.fitBounds([x1, y1, x2, y2], {animate: true, padding: 60});
      if (counter < steps) {
        requestAnimationFrame(animate);
      }
      counter = counter + 1;
    };

    function pointOnLine(coords: any): GeoJSON.Feature {
      return {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'Point',
          coordinates: coords
        }
      };
    }
  }

  removePolygon(): void {
    if (this.map.getLayer('finalPolygon')) {
      this.map.removeLayer('finalPolygon');
    }
    if (this.map.getLayer('outline')) {
      this.map.removeLayer('outline');
    }
    if (this.map.getSource('finalPolygon')) {
      this.map.removeSource('finalPolygon');
    }
  }

  createMarkerV2(): LatLon {
    this.marker.setLngLat([this.lng, this.lat]).addTo(this.map);
    return {lat: this.lat, lon: this.lng};
  }

  setMarkerOnDblClick(): void {
    this.map.on('dblclick', (e) => {
      this.marker.setLngLat(e.lngLat).addTo(this.map);
      this.to = [e.lngLat.lng, e.lngLat.lat];
      if (this.markerRed.getLngLat()) {
        this.markerRed.setLngLat([e.lngLat.lng + 0.0008, e.lngLat.lat + 0.0008]).addTo(this.map);
        this.from = [e.lngLat.lng + 0.0008, e.lngLat.lat + 0.0008];
      }
      e.preventDefault();
      this.distance = turf.distance(this.to, this.from);
    });
  }

  setMarkerOnGeocoderResult(): void {
    this.geocoder.on('result', (result) => {
      const resultCoord = result.result.center;
      this.marker.setLngLat(resultCoord).addTo(this.map);
      this.to = resultCoord;
      if (this.markerRed.getLngLat()) {
        this.markerRed.setLngLat([resultCoord[0] + 0.0008, resultCoord[1] + 0.0008]).addTo(this.map);
        this.from = [resultCoord[0] + 0.0008, resultCoord[1] + 0.0008];
      }
      this.distance = turf.distance(this.to, this.from);
    });
  }

  removeMarkerV2(): void {
    this.marker.remove();
  }

  createSecMarkerV2(): void {
    const lng = this.marker.getLngLat().lng;
    const lat = this.marker.getLngLat().lat;
    this.markerRed.setLngLat([lng + 0.0008, lat + 0.0008]).addTo(this.map);
  }

  removeSecMarkerV2(): void {
    this.markerRed.remove();
  }

  getDistance(): void {
    this.marker.on('drag', () => {
      const toLng = this.marker.getLngLat().lng;
      const toLat = this.marker.getLngLat().lat;
      this.to = [toLng, toLat];
      this.distance = turf.distance(this.to, this.from);
    });
    this.markerRed.on('drag', () => {
      const fromLng = this.markerRed.getLngLat().lng;
      const fromLat = this.markerRed.getLngLat().lat;
      this.from = [fromLng, fromLat];
      this.distance = turf.distance(this.to, this.from);
    });
  }

  getLngLat(): LatLon {
    return {lat: this.marker.getLngLat().lat, lon: this.marker.getLngLat().lng};
  }
}
