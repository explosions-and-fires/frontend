import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {ActiveToast, ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class InformerService {
  constructor(
    private toastr: ToastrService
  ) {
  }

  // tslint:disable-next-line:typedef
  public HandleError<T>(operation = 'Operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.error.optionalInfo.kibana_link){
        this.toastr.error(
          `<p>${error.error.error}</p>
                    <p>Информация по ошибке доступна по адресу:
                        <a href="${error.error.optionalInfo.kibana_link}">view log</a>
                    </p>`,
          `${operation}.` + ' Статус ' + `${error.error.status}`,
          {
            enableHtml: true,
            closeButton: true,
            timeOut: 10000
          });
      } else {
        this.toastr.error(`${error.error.error}`, `${operation}.` + ' Статус ' + `${error.error.status}`);
      }
      return of(result as T);
    };
  }

  public ShowInformation(message: string): ActiveToast<any>{
    return this.toastr.info(message, 'Дополнительная информация!');
  }

  public ErrorMessage(message: string): ActiveToast<any>{
    return this.toastr.error(message);
  }
}
