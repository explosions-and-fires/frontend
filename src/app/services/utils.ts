import {delay, mergeMap, take} from 'rxjs/operators';
import {of} from 'rxjs';

export function getStatusCode(err: any): number {
  return Math.floor((err.status / 100) % 10);
}

export function retryIfError(err: any, responseStat: number): any {
  let retries = 0;
  return err.pipe(
    // tslint:disable-next-line:no-shadowed-variable
    mergeMap((response: any) => {
      if (retries++ === 3) {
        console.log('Retrying exited in number ', retries);
        throw response;
      }
      if (response?.status === responseStat) {
        return of(response).pipe(
          delay(1500),
          take(3),
        );
      } else {
        throw response;
      }
    })
  );
}
