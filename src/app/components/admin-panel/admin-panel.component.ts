import { Component, OnInit } from '@angular/core';

interface Links {
  Title: string;
  Description: string;
  Logo: string;
  Link: string;
}

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {
  links: Links[] = [{
    Title: 'Portainer',
    Description: 'Система для работы с кластером',
    Logo: '../../../assets/tech/portainer.png',
    Link: 'https://portainer.exfire.space'
  },
    {
      Title: 'Gitlab',
      Description: 'Репозитории проекта',
      Logo: '../../../assets/tech/gitlab.png',
      Link: 'https://gitlab.com/explosions-and-fires'
    },
    {
      Title: 'Grafana',
      Description: 'Мониторинг состояния кластера',
      Logo: '../../../assets/tech/grafana.png',
      Link: 'https://grafana.exfire.space/d/W9_EOflGz/monitoring?orgId=1&refresh=5s'
    },
    {
      Title: 'Jira',
      Description: 'Таск-трекер',
      Logo: '../../../assets/tech/jira.png',
      Link: 'https://explosions-and-fires.atlassian.net/jira/software/projects/EFP/boards/1'
    },
    {
      Title: 'Kibana',
      Description: 'Система для работы с логами микросервисов',
      Logo: '../../../assets/tech/kibana.jpg',
      Link: 'https://kibana.exfire.space/'
    },
    {
      Title: 'RabbitMQ',
      Description: 'Контроль очередей в брокере сообщений',
      Logo: '../../../assets/tech/rabbitmq.png',
      Link: 'https://rabbitmq.exfire.space/'
    },
    {
      Title: 'Eureka',
      Description: 'Контроль запуска микросервисов',
      Logo: '../../../assets/tech/spring.png',
      Link: 'https://eureka.exfire.space/'
    },
    {
      Title: 'Traefik',
      Description: 'Мониторинг состояния хостов системы',
      Logo: '../../../assets/tech/traefik.jpg',
      Link: 'https://traefik.exfire.space/dashboard/#/'
    }];

  constructor() {
  }

  ngOnInit(): void {
  }
}
