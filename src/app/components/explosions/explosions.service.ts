import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {InformerService} from '../../services/informer.service';
import {Observable, of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError, map, retryWhen} from 'rxjs/operators';
import {retryIfError} from '../../services/utils';

export interface Substances {
  classification: number;
  detonationCellMin: string;
  detonationCellMax: number;
  beta: number;
  substance: string;
  condition: string;
}

export interface ExplosionsValues {
  classification?: number;
  beta?: number;
  condition?: string;
  env_area_type?: number;
  Mr?: number;
  N?: number;
  Cr?: number;
  Cst?: number;
  r?: number;
  lat?: number;
  lon?: number;
}

export interface AnswersResponse {
  V: {
    description: string,
    value: number
  };
  r: {
    description: string,
    value: number
  };
  Po: {
    description: string,
    value: number
  };
  deltaP: {
    description: string,
    value: number
  };
  I: {
    description: string;
    value: number;
  };
  damageRadiuses: [{
    characteristic: string;
    radius: {
      description: string,
      value: number
    };
    deltaP: {
      description: string,
      value: string
    };
  }];
  probits: [{
    description: string,
    value: number
  }];
}

export interface ResponseFromServer {
  uuid: string;
  responseQueueName: string;
}

@Injectable({
  providedIn: 'root'
})
export class ExplosionsService {

  constructor(
    private http: HttpClient,
    private informer: InformerService
  ) {
  }

  getSubstances(): Observable<Substances[]> {
    return this.http.get<HttpResponse<Substances[]>>(`${environment.basicURL}/specs/explosions/substances`, {
      observe: 'response' as 'body'
    }).pipe(
      retryWhen(errors => {
        return retryIfError(errors, 422);
      }),
      map((res: HttpResponse<Substances[]>) => {
        return res.body;
      }),
      catchError(() => {
        this.informer.HandleError<Substances[]>('Ошибка получения веществ');
        return of(null);
      })
    );
  }

  makeRequestForUUID(UUID: string, QUEUE: string): Observable<AnswersResponse> {
    return this.http.get<AnswersResponse>(`${environment.basicURL}/response`, {
      observe: 'body',
      params: {
        uuid: UUID,
        queue: QUEUE
      },
      headers: {
        'X-CORRELATION-ID': UUID
      }
    }).pipe(
      retryWhen(errors => {
        return retryIfError(errors, 422);
      }),
      catchError(this.informer.HandleError<AnswersResponse>('Ошибка получения расчетов'))
    );
  }

  makeRequestForTask(values: ExplosionsValues): Observable<ResponseFromServer> {
    return this.http.post<ResponseFromServer>(
      `${environment.basicURL}/calculate/async/explosions`, values,
      {
        observe: 'body'
      }
    ).pipe(
      catchError(this.informer.HandleError<ResponseFromServer>('Ошибка получения расчетов'))
    );
  }
}
