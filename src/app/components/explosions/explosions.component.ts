import {Component, DoCheck, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ExplosionsService, ExplosionsValues, Substances} from './explosions.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatStepper, MatStepperIntl} from '@angular/material/stepper';
import {Title} from '@angular/platform-browser';
import {LatLon, MapService} from '../../services/map.service';
import {InformerService} from '../../services/informer.service';
import {Observable, of, ReplaySubject} from 'rxjs';
import {catchError, finalize, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-explosions',
  templateUrl: './explosions.component.html',
  styleUrls: ['./explosions.component.scss']
})
export class ExplosionsComponent implements OnInit, OnDestroy, DoCheck {
  constructor(
    private matStep: MatStepperIntl,
    private titleService: Title,
    private http: ExplosionsService,
    private map: MapService,
    private informer: InformerService,
  ) {
    this.sfg1 = new FormGroup({
      substance: new FormControl({value: null, disabled: false}, [Validators.required]),
      condition: new FormControl({value: null, disabled: true}, [Validators.required]),
      N: new FormControl({value: null, disabled: true}, [Validators.required]),
      Cr: new FormControl(null, [Validators.required]),
      Cst: new FormControl(null, [Validators.required]),
      env_area_type: new FormControl(null, [Validators.required]),
      Mr: new FormControl(null, [Validators.required]),
      objectDistance: new FormControl(0, [Validators.required])
    });
  }

  @ViewChild('stepper')
  stepper: MatStepper;

  spin = false;
  substance = false;
  Cr = false;
  selectedSubstance = false;
  showResult = false;
  showWarning = false;

  sfg1: FormGroup;

  latLong: LatLon;

  condition: string;
  detonationCellMin: number;
  detonationCellMax: number;

  V: {
    description: string,
    value: number
  };
  r: {
    description: string,
    value: number
  };
  Po: {
    description: string,
    value: number
  };
  deltaP: {
    description: string,
    value: number
  };
  I: {
    description: string,
    value: number
  };
  damageRadiuses: [{
    characteristic: string;
    radius: {
      description: string,
      value: number
    };
    deltaP: {
      description: string,
      value: string
    };
  }];
  probits: [{
    description: string,
    value: number
  }];
  private explosionsValues: ExplosionsValues = {
    classification: null,
    beta: null,
    condition: null,
    env_area_type: null,
    Mr: null,
    N: null,
    Cr: null,
    Cst: null,
    r: null,
    lat: null,
    lon: null,
  };

  private destroy$ = new ReplaySubject<void>();
  substances$: Observable<Substances[]>;

  ngOnInit(): void {
    this.titleService.setTitle('Расчёт последствий взрывов');
    this.getSubstance();
    this.map.resetMap(true);
  }

  getSubstance(): void {
    if (!this.substances$) {
      this.spin = true;
      this.substances$ = this.http.getSubstances().pipe(
        finalize(() => {
          this.spin = false;
          this.latLong = this.map.createMarkerV2();
          this.map.createSecMarkerV2();
        }),
        takeUntil(this.destroy$),
        catchError(() => {
          return of([]);
        })
      );
    }
  }

  selected(event: any): void {
    if (event) {
      this.explosionsValues.classification = event.classification;
      this.explosionsValues.beta = event.beta;
      this.explosionsValues.condition = event.condition;
      if (event.condition === 'г') {
        this.condition = 'газ';
      } else {
        this.condition = 'гетроген';
      }
      this.detonationCellMin = event?.detonationCellMin;
      this.detonationCellMax = event?.detonationCellMax;
      this.selectedSubstance = true;
    } else {
      this.explosionsValues.classification = 1;
      this.explosionsValues.beta = 1;
    }
  }

  makeTaskOne(): void {
    this.map.flyToMarker();
    this.latLong = this.map.getLngLat();
    this.explosionsValues.lat = this.latLong.lat;
    this.explosionsValues.lon = this.latLong.lon;
    this.explosionsValues.N = this.sfg1.controls.N.value;
    this.explosionsValues.Cr = this.sfg1.controls.Cr.value;
    this.explosionsValues.Cst = this.sfg1.controls.Cst.value;
    this.explosionsValues.env_area_type = this.sfg1.controls.env_area_type.value;
    this.explosionsValues.Mr = this.sfg1.controls.Mr.value;
    this.explosionsValues.r = this.sfg1.controls.objectDistance.value;
    if (this.selectedSubstance === false) {
      this.explosionsValues.classification = 1;
      this.explosionsValues.beta = 1;
      this.explosionsValues.condition = this.sfg1.controls.condition.value;
    }
    this.http.makeRequestForTask(
      this.explosionsValues
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по определению количества вещества по первичному и вторичному облаку.');
        } else {
          this.V = values.V;
          this.V.description = values.V.description;
          this.V.value = values.V.value;

          this.r = values.r;
          this.r.description = values.r.description;
          this.r.value = values.r.value;

          this.Po = values.Po;
          this.Po.description = values.Po.description;
          this.Po.value = values.Po.value;

          this.deltaP = values.deltaP;
          this.deltaP.description = values.deltaP.description;
          this.deltaP.value = values.deltaP.value;

          this.I = values.I;
          this.I.description = values.I.description;
          this.I.value = values.I.value;

          this.damageRadiuses = values.damageRadiuses;
          this.probits = values.probits;
          this.map.createCircles(this.damageRadiuses);
          this.map.setRedMarkerPopup(this.probits);

          this.showResult = true;
        }
      }, () => {
        this.informer.HandleError('Ошибка получения расчета');
      });
    }, () => {
      this.informer.HandleError('Ошибка получения расчета');
    });
  }

  ngDoCheck(): void {
    if (this.substance) {
      this.selectedSubstance = false;
      this.sfg1.controls.substance.setValue('');
      this.sfg1.controls.substance.disable();
      this.sfg1.controls.condition.enable();
    } else {
      this.sfg1.controls.substance.enable();
      this.sfg1.controls.condition.disable();
    }
    if (this.Cr) {
      this.sfg1.controls.N.enable();
      this.sfg1.controls.Cr.disable();
    } else {
      this.sfg1.controls.N.disable();
      this.sfg1.controls.Cr.enable();
    }

    if (this.sfg1.controls.Cr.value < this.sfg1.controls.Cst.value) {
      this.showWarning = true;
    } else {
      this.showWarning = false;
    }
    this.sfg1.controls.objectDistance.setValue(parseFloat(this.map.distance.toFixed(3)));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
