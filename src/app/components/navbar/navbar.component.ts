import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../../auth.guard';
import { AdminGuard } from '../../admin.guard';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, DoCheck {
  constructor(
    private router: Router,
    private authGuard: AuthGuard,
    private adminGuard: AdminGuard
  ) {
  }

  routeName = '';
  visibility = false;
  userBtnVisibility = false;

  ngOnInit(): void {

  }

  ngDoCheck(): void {
    if (this.authGuard.isLoggedIn()) {
      this.visibility = true;
      if (this.adminGuard.isAdminUser()) {
        this.userBtnVisibility = true;
      }
    } else {
      this.visibility = false;
      this.userBtnVisibility = false;
    }
    this.getActiveRouteName();
  }

  getActiveRouteName(): void {
    this.router.config.forEach(item => {
      if (item.children !== undefined && item.children.length !== 0) {
        item.children.forEach(value => {
          if (`/${item.path}/${value.path}`.includes(this.router.url)) {
            this.routeName = value.data?.name;
          }
        });
      } else {
        if (`/${item.path}`.includes(this.router.url)) {
          this.routeName = item.data?.name;
        }
      }
    });
    if (this.routeName === undefined || this.routeName === '') {
      this.routeName = 'ExFire';
    }
    if (window.innerWidth <= 460) {
      this.routeName = 'ExFire';
    }
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
