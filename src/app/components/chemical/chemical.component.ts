import { Component, DoCheck, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, of, ReplaySubject } from 'rxjs';
import { MatStepper, MatStepperIntl } from '@angular/material/stepper';
import { Title } from '@angular/platform-browser';
import { ChemicalService, Gases, TaskValues } from './chemical.service';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { LatLon, MapService } from '../../services/map.service';
import { InformerService } from '../../services/informer.service';

@Component({
  selector: 'app-chemical',
  templateUrl: './chemical.component.html',
  styleUrls: ['./chemical.component.scss'],
  providers: [ChemicalService]
})
export class ChemicalComponent implements OnInit, OnDestroy, DoCheck {
  constructor(
    private matStep: MatStepperIntl,
    private titleService: Title,
    private http: ChemicalService,
    private map: MapService,
    private informer: InformerService,
  ) {
    this.sfg1 = new FormGroup({
      typeSdyav: new FormControl(null),
      stateSdyav: new FormControl(null),
      amountSubstanceInGas: new FormControl(null),
      volumeSectionBetweenValves: new FormControl(null),
      storageCapacity: new FormControl(null),
      whereSpillOccurred: new FormControl(null, [Validators.required]),
      palletHeight: new FormControl(null),
      areaOfSpill: new FormControl(null),
      amountOfSubstanceReleased: new FormControl(null),
      timeAfterCrash: new FormControl(null, [Validators.required]),
    });
    this.sfg2 = new FormGroup({});
    this.sfg3 = new FormGroup({
      namesCtrl: new FormControl(null),
      timeAfterCrash: new FormControl(null, [Validators.required])
    });
    this.sfg4 = new FormGroup({});
    this.sfg5 = new FormGroup({
      objectDistance: new FormControl(null, [Validators.required]),
    });
    this.sfg6 = new FormGroup({
      names: new FormControl(null, [Validators.required])
    });
  }

  @ViewChild('stepper')
  stepper: MatStepper;
  @ViewChild('optionalStepper')
  optionalStepper: MatStepper;
  spin = false;
  gases$: Observable<Gases[]>;
  private destroy$ = new ReplaySubject<void>();
  sfg1: FormGroup;
  sfg2: FormGroup;
  sfg3: FormGroup;
  sfg4: FormGroup;
  sfg5: FormGroup;
  sfg6: FormGroup;
  q0 = new FormArray([], [Validators.required]);
  stateSdyav = new FormArray([], [Validators.required]);
  makeMarkerOnMap = false;
  makeSecMarkerOnMap = false;
  isLiquefied = false;
  noSdyav = false;
  thereIsAmount = false;
  distance: number;
  // tslint:disable-next-line:variable-name
  first_air: number;
  // tslint:disable-next-line:variable-name
  second_air: number;
  showResultForTaskOne = false;
  showResultForTaskTwo = false;
  showResultForTaskThree = false;
  showResultForTaskFour = false;
  showResultForTaskFive = false;
  showResultForTaskSix = false;
  removable = true;
  // task 2
  deptZoneTrue: number;
  mapsZone: number;
  mapsAngle: number;
  // tslint:disable-next-line:variable-name
  wind_degree: number;
  // tslint:disable-next-line:variable-name
  area_Infection_true: number;
  // tslint:disable-next-line:variable-name
  area_infection_possible: number;
  // tslint:disable-next-line:variable-name
  time_to_dept: number;
  // tslint:disable-next-line:variable-name
  time_infection_action_first: number;
  // tslint:disable-next-line:variable-name
  time_infection_action_second: number;
  deptZonePossible: number;
  latLong: LatLon;
  names = [];
  private taskOneValuesResult: TaskValues = {
    task: 1
  };

  ngOnInit(): void {
    this.matStep.optionalLabel = 'Необязательный расчёт';
    this.titleService.setTitle('Расчёт заражения сильно ядовитыми веществами');
    this.spin = true;
    this.map.resetMap(true);
    this.getSdyav();
  }

  getSdyav(): void {
    this.gases$ = this.http.getAllGases().pipe(
      finalize(() => {
        this.spin = false;
        this.latLong = this.map.createMarkerV2();
      }),
      takeUntil(this.destroy$),
      catchError(() => {
        return of([]);
      })
    );
  }

  selected(event: any): void {
    const obj = {
      gas_id: null,
      name: null,
      q0: null,
      storage_type: null,
      type_sdyw: false,
      lat: null,
      lon: null,
      v_g: undefined,
      v_w: undefined
    };
    obj.name = event.name;
    obj.gas_id = event.id;
    obj.v_g = event?.v_g;
    obj.v_w = event?.v_w;
    this.q0.push(new FormControl(null, [Validators.required]));
    this.stateSdyav.push(new FormControl(null, [Validators.required]));
    this.names.push(obj);
    this.sfg3.controls.namesCtrl.setValue('');
    this.sfg3.controls.namesCtrl.setValue(null);
  }

  remove(item: string): void {
    const index = this.names.indexOf(item);
    if (index >= 0) {
      this.names.splice(index, 1);
      this.q0.removeAt(index);
      this.stateSdyav.removeAt(index);
    }
  }

  makeTaskOne(): void {
    const taskOneValues: TaskValues = {
      task: 1
    };
    this.spin = true;
    if (this.noSdyav) {
      taskOneValues.type_sdyw = false;
      taskOneValues.storage_type = 2;
      if (this.sfg1.controls.sdyavDensity?.value !== null) {
        taskOneValues.v_w = parseFloat(this.sfg1.controls.sdyavDensity?.value);
      }
    } else {
      taskOneValues.v_g = this.sfg1.controls.typeSdyav.value?.v_g;
      taskOneValues.v_w = this.sfg1.controls.typeSdyav.value?.v_w;
    }
    switch (this.sfg1.controls.stateSdyav.value) {
      case 'gas': {
        taskOneValues.type_sdyw = true;
        break;
      }
      case 'liquid': {
        taskOneValues.type_sdyw = false;
        break;
      }
    }
    if (this.isLiquefied === true) {
      taskOneValues.storage_type = 2;
    } else {
      switch (this.sfg1.controls.stateSdyav.value) {
        case 'gas': {
          taskOneValues.storage_type = 3;
          break;
        }
        case 'liquid': {
          taskOneValues.storage_type = 1;
          break;
        }
      }
    }
    switch (this.sfg1.controls.whereSpillOccurred.value) {
      case 'freeSpillage': {
        taskOneValues.type_spill = 1;
        break;
      }
      case 'intoPallet': {
        taskOneValues.type_spill = 2;
        break;
      }
      case 'intoCommonPallet': {
        taskOneValues.type_spill = 3;
        break;
      }
    }
    if (this.sfg1.controls.storageCapacity.value !== null) {
      taskOneValues.Vx = parseFloat(this.sfg1.controls.storageCapacity.value);
    }
    if (this.sfg1.controls.amountSubstanceInGas.value !== null) {
      taskOneValues.n = parseFloat(this.sfg1.controls.amountSubstanceInGas.value);
    }
    if (this.sfg1.controls.volumeSectionBetweenValves.value !== null) {
      taskOneValues.Vr = parseFloat(this.sfg1.controls.volumeSectionBetweenValves.value);
    }
    if (this.sfg1.controls.palletHeight.value !== null) {
      taskOneValues.H = parseFloat(this.sfg1.controls.palletHeight.value);
    }
    if (this.sfg1.controls.areaOfSpill.value !== null) {
      taskOneValues.S_razl = parseFloat(this.sfg1.controls.areaOfSpill.value);
    }
    if (this.sfg1.controls.steamPressure?.value !== null) {
      taskOneValues.P = parseFloat(this.sfg1.controls.steamPressure?.value);
    }
    if (this.sfg1.controls.molecularMass?.value !== null) {
      taskOneValues.M = parseFloat(this.sfg1.controls.molecularMass?.value);
    }
    if (this.sfg1.controls.nameSdyavNT5?.value !== null) {
      taskOneValues.name = this.sfg1.controls.nameSdyavNT5?.value;
    }
    if (this.sfg1.controls.timeAfterCrash.value !== null) {
      taskOneValues.N = parseFloat(this.sfg1.controls.timeAfterCrash.value);
    }
    if (this.sfg1.controls.specificHeat?.value !== null) {
      taskOneValues.Cp = parseFloat(this.sfg1.controls.specificHeat?.value);
    }
    if (this.sfg1.controls.temperatureDifference?.value !== null) {
      taskOneValues.T = parseFloat(this.sfg1.controls.temperatureDifference?.value);
    }
    if (this.sfg1.controls.heatOfVaporization?.value !== null) {
      taskOneValues.H_eva = parseFloat(this.sfg1.controls.heatOfVaporization?.value);
    }
    if (this.sfg1.controls.threshold_toxodosis?.value !== null) {
      taskOneValues.threshold_toxodosis = parseFloat(this.sfg1.controls.threshold_toxodosis?.value);
    }
    this.latLong = this.map.getLngLat();
    taskOneValues.gas_id = this.sfg1.controls.typeSdyav.value?.id;
    taskOneValues.q0 = parseFloat(this.sfg1.controls.amountOfSubstanceReleased.value);
    taskOneValues.isTableFive = !this.noSdyav;
    taskOneValues.lat = this.latLong.lat;
    taskOneValues.lon = this.latLong.lon;
    this.http.makeRequestForTask(
      taskOneValues
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по определению количества вещества по первичному и вторичному облаку.');
        } else {
          this.showResultForTaskOne = true;
          this.first_air = values.first_air;
          this.second_air = values.second_air;
          this.taskOneValuesResult = taskOneValues;
        }
      }, () => {
        this.informer.HandleError('Ошибка получения расчета');
      });
    }, () => {
      this.informer.HandleError('Ошибка получения расчета');
    });
  }

  makeTaskTwo(): void {
    this.map.flyToMarker();
    this.spin = true;
    const taskTwoValues = this.taskOneValuesResult;
    taskTwoValues.task = 2;
    this.http.makeRequestForTask(
      taskTwoValues
    ).pipe(
      takeUntil(this.destroy$),
      finalize(() => {
        this.spin = false;
      }),
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по расчёту глубины заражения по первичному облаку.', 'ОШИБКА');
        } else {
          this.showResultForTaskTwo = true;
          this.deptZoneTrue = values.dept_zone_true;
          this.mapsZone = values.maps_zone;
          this.mapsAngle = values.maps_Angle;
          this.wind_degree = values.wind_degree;
          this.map.createSector(values.dept_zone_true, values.maps_Angle, values.wind_degree);
        }
      }, error => {
        this.informer.HandleError(error.error);
      });
    }, error => {
      this.informer.HandleError(error.error);
    });
  }

  makeTaskThree(): void {
    this.map.flyToMarker();
    this.spin = true;
    for (let i = 0; i < this.q0.length; i++) {
      this.names[i].q0 = parseFloat(this.q0.controls[i].value);
      switch (this.stateSdyav.controls[i].value) {
        case 'gas': {
          this.names[i].storage_type = 3;
          this.names[i].type_sdyw = true;
          break;
        }
        case 'liquid': {
          this.names[i].storage_type = 1;
          this.names[i].type_sdyw = false;
          break;
        }
        case 'liquidComp': {
          this.names[i].storage_type = 2;
          this.names[i].type_sdyw = false;
          break;
        }
      }
    }
    const taskThreeValues: TaskValues = {
      task: 3,
      N: this.sfg3.controls.timeAfterCrash.value,
      lat: this.latLong.lat,
      lon: this.latLong.lon,
      names: this.names,
    };
    this.http.makeRequestForTask(taskThreeValues).pipe(
      takeUntil(this.destroy$),
      finalize(() => {
        this.spin = false;
      }),
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по расчёту глубины заражения по первичному облаку.', 'ОШИБКА');
        } else {
          this.showResultForTaskThree = true;
          this.deptZonePossible = values.dept_zone_possible;
          this.mapsAngle = values.maps_Angle;
          this.wind_degree = values.wind_degree;
          this.map.createSector(this.deptZonePossible, this.mapsAngle, this.wind_degree);
        }
      }, error => {
        this.informer.HandleError(error.error);
      });
    }, error => {
      this.informer.HandleError(error.error);
    });
  }

  makeTaskFour(): void {
    this.spin = true;
    this.taskOneValuesResult.task = 4;
    this.http.makeRequestForTask(
      this.taskOneValuesResult
    ).pipe(
      takeUntil(this.destroy$),
      finalize(() => {
        this.spin = false;
      }),
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по расчёту глубины заражения по первичному облаку.', 'ОШИБКА');
        } else {
          this.showResultForTaskFour = true;
          this.area_Infection_true = values.area_Infection_true;
          this.area_infection_possible = values.area_infection_possible;
        }
      }, error => {
        this.informer.HandleError(error.error);
      });
    }, error => {
      this.informer.HandleError(error.error);
    });
  }

  makeTaskFive(): void {
    this.spin = true;
    const x = this.sfg5.controls.objectDistance.value;
    this.latLong = this.map.getLngLat();
    const lat = this.latLong.lat;
    const lon = this.latLong.lon;
    const taskFiveValues: TaskValues = {
      task: 5,
      x,
      lat,
      lon
    };
    this.http.makeRequestForTask(
      taskFiveValues
    ).pipe(
      takeUntil(this.destroy$),
      finalize(() => {
        this.spin = false;
      }),
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по расчёту глубины заражения по первичному облаку.', 'ОШИБКА');
        } else {
          this.showResultForTaskFive = true;
          this.time_to_dept = values.time_to_dept_object;
        }
      }, error => {
        this.informer.HandleError(error.error);
      });
    }, error => {
      this.informer.HandleError(error.error);
    });
  }

  makeTaskSix(): void {
    this.spin = true;
    this.taskOneValuesResult.task = 6;
    this.http.makeRequestForTask(
      this.taskOneValuesResult
    ).pipe(
      takeUntil(this.destroy$),
      finalize(() => {
        this.spin = false;
      }),
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по расчёту глубины заражения по первичному облаку.', 'ОШИБКА');
        } else {
          this.showResultForTaskSix = true;
          this.time_infection_action_first = values.time_infection_action_first;
          this.time_infection_action_second = values.time_infection_action_second;
        }
      }, error => {
        this.informer.HandleError(error.error);
      });
    }, error => {
      this.informer.HandleError(error.error);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  setFirstMarker(): void {
    if (!this.makeMarkerOnMap) {
      this.latLong = this.map.createMarkerV2();
    } else {
      this.map.removeMarkerV2();
    }
  }

  setSecondMarker(): void {
    if (!this.makeSecMarkerOnMap) {
      this.map.createSecMarkerV2();
    } else {
      this.map.removeSecMarkerV2();
      this.map.distance = 0;
    }
  }

  resetForm(): void {
    this.sfg1.reset();
    this.isLiquefied = false;
    this.thereIsAmount = false;
  }

  noSdyavValid(): void {
    const task1 = this.sfg1;
    if (!this.noSdyav) {
      task1.controls.typeSdyav.setValidators(Validators.required);
      if (task1.controls.typeSdyav.value != null || undefined) {
        task1.controls.stateSdyav.setValidators(Validators.required);
      } else {
        task1.controls.stateSdyav.clearValidators();
      }
      if (task1.controls.stateSdyav.value === 'gas') {
        task1.controls.amountSubstanceInGas.setValidators(Validators.required);
        task1.controls.volumeSectionBetweenValves.setValidators(Validators.required);
      } else {
        task1.controls.amountSubstanceInGas.clearValidators();
        task1.controls.volumeSectionBetweenValves.clearValidators();
      }
      if (this.thereIsAmount || task1.controls.stateSdyav.value === 'liquid' && !this.isLiquefied) {
        task1.controls.amountOfSubstanceReleased.setValidators(Validators.required);
      } else {
        task1.controls.amountOfSubstanceReleased.clearValidators();
      }
      if (task1.controls.whereSpillOccurred.value) {
        switch (task1.controls.whereSpillOccurred.value) {
          case 'intoPallet': {
            task1.controls.palletHeight.setValidators(Validators.required);
            break;
          }
          case 'intoCommonPallet': {
            task1.controls.areaOfSpill.setValidators(Validators.required);
            break;
          }
        }
      } else {
        task1.controls.palletHeight.clearValidators();
        task1.controls.areaOfSpill.clearValidators();
      }
      task1.removeControl('nameSdyavNT5');
      task1.removeControl('steamPressure');
      task1.removeControl('molecularMass');
      task1.removeControl('specificHeat');
      task1.removeControl('temperatureDifference');
      task1.removeControl('heatOfVaporization');
      task1.removeControl('sdyavDensity');
      task1.removeControl('threshold_toxodosis');
    } else {
      task1.addControl('nameSdyavNT5', new FormControl(null, [Validators.required]));
      task1.addControl('steamPressure', new FormControl(null, [Validators.required]));
      task1.addControl('molecularMass', new FormControl(null, [Validators.required]));
      task1.addControl('specificHeat', new FormControl(null, [Validators.required]));
      task1.addControl('temperatureDifference', new FormControl(null, [Validators.required]));
      task1.addControl('heatOfVaporization', new FormControl(null, [Validators.required]));
      task1.addControl('sdyavDensity', new FormControl(null, [Validators.required]));
      task1.addControl('threshold_toxodosis', new FormControl(null, [Validators.required]));
      task1.controls.typeSdyav.clearValidators();
    }
    if (this.isLiquefied || this.noSdyav) {
      task1.controls.storageCapacity.setValidators(Validators.required);
    } else {
      task1.controls.storageCapacity.clearValidators();
    }
  }

  ngDoCheck(): void {
    if (this.noSdyav) {
      this.sfg1.controls.typeSdyav.setValue('');
      this.sfg1.controls.typeSdyav.disable();
    } else {
      this.sfg1.controls.typeSdyav.enable();
    }
    if (this.thereIsAmount) {
      this.isLiquefied = false;
    }
    this.noSdyavValid();
    this.sfg5.controls.objectDistance.setValue(this.map.distance.toFixed(3));
  }
}
