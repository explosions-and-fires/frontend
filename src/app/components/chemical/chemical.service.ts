import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, retryWhen } from 'rxjs/operators';
import { ErrorsService } from '../../services/errors.service';
import { InformerService } from '../../services/informer.service';
import { environment } from '../../../environments/environment';
import { retryIfError } from '../../services/utils';

export interface Gases {
  id: number;
  name: string;
  v_g: number;
  v_w: number;
}

export interface AnswersResponse {
  first_air: number;
  second_air: number;
  dept_zone_true: number;
  maps_zone: number;
  maps_Angle: number;
  wind_degree: number;
  area_Infection_true: number;
  area_infection_possible: number;
  time_to_dept_object: number;
  time_infection_action_first: number;
  time_infection_action_second: number;
  dept_zone_possible: number;
}

export interface ResponseFromServer {
  uuid: string;
  responseQueueName: string;
}

export interface TaskValues {
  task: number;
  gas_id?: number;
  lat?: number;
  lon?: number;
  type_sdyw?: boolean;
  storage_type?: number;
  Vx?: number;
  n?: number;
  Vr?: number;
  q0?: number;
  type_spill?: number;
  H?: number;
  S_razl?: number;
  P?: number;
  M?: number;
  isTableFive?: boolean;
  threshold_toxodosis?: number;
  name?: any;
  names?: any[];
  N?: number;
  Cp?: number;
  T?: number;
  H_eva?: number;
  v_g?: number;
  v_w?: number;
  x?: number;
}

@Injectable()
export class ChemicalService {
  constructor(
    private http: HttpClient,
    private errs: ErrorsService,
    private informer: InformerService
  ) {
  }

  headers: HttpHeaders = new HttpHeaders();

  getAllGases(): Observable<Gases[]> {
    return this.http.get<HttpResponse<Gases[]>>(`${environment.basicURL}/specs/chemical`, {
      observe: 'response' as 'body'
    }).pipe(
      map((res: HttpResponse<Gases[]>) => {
        return res.body;
      }),
      catchError(() => {
        this.informer.HandleError<Gases[]>('Ошибка получения веществ');
        return of(null);
      })
    );
  }

  makeRequestForUUID(UUID: string, QUEUE: string): Observable<AnswersResponse> {
    return this.http.get<AnswersResponse>(`${environment.basicURL}/response`, {
      observe: 'body',
      params: {
        uuid: UUID,
        queue: QUEUE
      },
      headers: {
        'X-CORRELATION-ID': UUID
      }
    }).pipe(
      retryWhen(errors => {
        return retryIfError(errors, 422);
      }),
      catchError(this.informer.HandleError<AnswersResponse>('Ошибка получения расчетов'))
    );
  }

  makeRequestForTask(task: TaskValues): Observable<ResponseFromServer> {
    return this.http.post<ResponseFromServer>(
      `${environment.basicURL}/calculate/async/chemical`, task,
      {
        observe: 'body'
      }
    ).pipe(
      catchError(this.informer.HandleError<ResponseFromServer>('Ошибка получения расчетов'))
    );
  }
}

