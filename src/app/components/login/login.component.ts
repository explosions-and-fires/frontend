import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {LoginService} from './login.service';
import {finalize, takeUntil} from 'rxjs/operators';
import {Title} from '@angular/platform-browser';
import {getStatusCode} from '../../services/utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private http: LoginService,
    private titleService: Title,
  ) {
  }

  private destroy$ = new Subject<void>();
  loginForm: FormGroup;
  hide = true;
  error = false;
  unavailable = false;
  spin = false;

  ngOnInit(): void {
    this.titleService.setTitle('Авторизация');
    this.loginForm = new FormGroup({
      login: new FormControl('', [Validators.required, Validators.minLength(4)]),
      pass: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  login(): void {
    this.spin = true;
    this.http.login(
      this.loginForm.value.login,
      this.loginForm.value.pass
    ).pipe(
      finalize(() => {
        this.spin = false;
        this.loginForm.setValue({login: '', pass: ''});
      }),
      takeUntil(this.destroy$)
    ).subscribe((res) => {
      if (res !== undefined && res.token !== '') {
        localStorage.setItem('roles', `${res.roles}`);
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('token', res.token);
        this.router.navigate(['/main']);
      } else {
        this.error = true;
      }
    }, (err) => {
      const statusCode = getStatusCode(err);
      if (statusCode === 4){
        this.error = true;
      } else if (statusCode === 5) {
        this.unavailable = true;
      } else {
        this.unavailable = true;
      }
    });
  }

  register(): void {
    this.router.navigate(['register']);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
