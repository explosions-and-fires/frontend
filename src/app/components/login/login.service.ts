import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retryWhen } from 'rxjs/operators';
import { InformerService } from '../../services/informer.service';
import { environment } from '../../../environments/environment';
import { retryIfError } from '../../services/utils';

interface Token {
  token: string;
  username: string;
  roles: string[];
}

@Injectable()
export class LoginService {
  constructor(
    private http: HttpClient,
    private informer: InformerService
  ) {
  }

  login(login: string, pass: string): Observable<Token> {
    return this.http.post<Token>(`${environment.basicURL}/login`, {
      username: login,
      password: pass
    }, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).pipe(
      retryWhen(errors => {
        return retryIfError(errors, 500);
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
