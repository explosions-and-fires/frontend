import { Component, OnDestroy, OnInit } from '@angular/core';
import { MapService } from '../../services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  selectedStyle = 'streets-v11';
  styles = [
    {
      name: 'Улицы',
      style: 'streets-v11'
    },
    {
      name: 'Наружная',
      style: 'outdoors-v11'
    },
    {
      name: 'Спутник',
      style: 'satellite-v9'
    }
  ];
  xcoord: number;
  ycoord: number;
  type: string;
  u1: string;
  u2: string;
  distance: number;

  canAnimate = false;

  onChange(): void {
    this.map.switchLayer(this.selectedStyle);
    this.map.switchLanguage();
  }

  constructor(public map: MapService) {
  }

  ngOnInit(): void {
    this.map.buildMap();
    this.map.markerCallBack = () => {
      this.ycoord = this.map.marker.getLngLat().lat;
      this.xcoord = this.map.marker.getLngLat().lng;
      this.distance = this.map.distance;
    };
  }

  clearMap(completeReset: boolean): void {
    this.map.resetMap(completeReset);
  }

  ngOnDestroy(): void {
    this.map.map.remove();
  }
}
