import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

interface Task {
  Title: string;
  Link: string;
  Tasks: string[];
  Photo: string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  constructor(
    private auth: AuthService
  ) {
  }

  tasks: Task[] = [
    {
      Title: 'Последствия лесных пожаров',
      Link: 'fires',
      Tasks: [
        'Определение скорости распространения пожара',
        'Определение размеров пожара',
        'Определение количества непригодной к реализации древесины',
        'Определение степени повреждения древостоя после низовых пожаров'
      ],
      Photo: '../../../assets/fires.jpg'
    },
    {
      Title: 'Последствия заражения СДЯВ',
      Link: 'chemical',
      Tasks: [
        'Определение количества вещества по первичному и вторичному облаку',
        'Определение глубины зоны заражения',
        'Определение площади заражения',
        'Определение продолжительности поражающего действия СДЯВ',
        'Определение глубины зоны возможного заражения',
        'Определение времени подхода зараженного воздуха к объекту'
      ],
      Photo: '../../../assets/chemical_bg3.jpg'
    },
    {
      Title: 'Последствия взрывов ТВС',
      Link: 'explosions',
      Tasks: [
        'Оценка последствий аварийных взрывов топливно-воздушных смесей'
      ],
      Photo: '../../../assets/explosions2.jpg'
    }
  ];

  ngOnInit(): void {
    this.auth.checkIfTokenValid();
  }
}
