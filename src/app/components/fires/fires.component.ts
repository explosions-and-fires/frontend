import { Component, DoCheck, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, of, ReplaySubject } from 'rxjs';
import { MatStepper, MatStepperIntl } from '@angular/material/stepper';
import { Title } from '@angular/platform-browser';
import { FiresService } from './fires.service';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { LatLon, MapService } from '../../services/map.service';
import { InformerService } from '../../services/informer.service';
import { FiresTaskValues } from './fires.service';

@Component({
  selector: 'app-fires',
  templateUrl: './fires.component.html',
  styleUrls: ['./fires.component.scss']
})
export class FiresComponent implements OnInit, DoCheck, OnDestroy {
  constructor(
    private matStep: MatStepperIntl,
    private titleService: Title,
    private http: FiresService,
    private map: MapService,
    private informer: InformerService,
  ) {
    this.sfg1 = new FormGroup({
      fire_class: new FormControl(null, [Validators.required]),
      fire_type: new FormControl(null, [Validators.required])
    });
    this.sfg2 = new FormGroup({
      time: new FormControl(null, [Validators.required]),
      p0: new FormControl(null, [Validators.required]),
      area: new FormControl(null, [Validators.required])
    });
    this.sfg3 = new FormGroup({
      tree_kind: new FormControl(null, [Validators.required]),
      fire_kind: new FormControl(null, [Validators.required])
    });
    this.sfg4 = new FormGroup({
      tree_kind: new FormControl(null, [Validators.required]),
      tree_diameter: new FormControl(null, [Validators.required]),
      nagar_height: new FormControl(null, [Validators.required])
    });
  }

  @ViewChild('stepper')
  stepper: MatStepper;
  @ViewChild('optionalStepper')
  optionalStepper: MatStepper;
  sfg1: FormGroup;
  sfg2: FormGroup;
  sfg3: FormGroup;
  sfg4: FormGroup;
  private destroy$ = new ReplaySubject<void>();
  private taskOneValuesResult: FiresTaskValues = {
    task: 1
  };
  unsuitableFires$: Observable<string[]>;
  unsuitableTrees$: Observable<string[]>;
  damagedTrees$: Observable<string[]>;
  spin = false;
  showResultForTaskOne = false;
  showResultForTaskTwo = false;
  showResultForTaskThree = false;
  showResultForTaskFour = false;
  latLong: LatLon;
  // Task 1
  backSpeed: number;
  flangSpeed: number;
  frontSpeed: number;
  // Task 2
  perimeter: number;
  area: number;
  // Task 3
  count: number;
  // Task 4
  damageDegree: number;
  property: string;
  dropOffTrees: number;
  dropOfReserve: number;

  ngOnInit(): void {
    this.titleService.setTitle('Расчёт распространения лесных пожаров');
    this.matStep.optionalLabel = 'Необязательный расчёт';
    this.map.resetMap(true);
    this.map.createMarkerV2();
    this.getFiresAndTrees();
    this.getDamagedTrees();
  }

  getFiresAndTrees(): void {
    this.unsuitableFires$ = this.http.getUnsuitableFires().pipe(
      finalize(() => {
        this.spin = false;
      }),
      takeUntil(this.destroy$),
      catchError(() => {
        return of([]);
      })
    );
    this.unsuitableTrees$ = this.http.getUnsuitableTrees().pipe(
      finalize(() => {
        this.spin = false;
      }),
      takeUntil(this.destroy$),
      catchError(() => {
        return of([]);
      })
    );
  }

  getDamagedTrees(): void {
    this.damagedTrees$ = this.http.getDamagedTrees().pipe(
      finalize(() => {
        this.spin = false;
      }),
      takeUntil(this.destroy$),
      catchError(err => {
        return of([]);
      })
    );
  }

  perimeterOrAreaValid(): void {
    if (this.sfg1.controls.perimeter.value !== null || undefined) {
      this.sfg1.controls.Area.clearValidators();
    } else {
      this.sfg1.controls.Area.setValidators(Validators.required);
    }
    if (this.sfg1.controls.Area.value !== null || undefined) {
      this.sfg1.controls.perimeter.clearValidators();
    } else {
      this.sfg1.controls.perimeter.setValidators(Validators.required);
    }
  }

  makeTaskOne(): void {
    this.spin = true;
    this.latLong = this.map.getLngLat();
    const taskOneValues: FiresTaskValues = {
      task: 1,
      fire_type: this.sfg1.controls.fire_type.value,
      fire_class: this.sfg1.controls.fire_class.value,
      lat: this.latLong.lat,
      lon: this.latLong.lon
    };
    this.http.makeRequestForTask(
      taskOneValues
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по определению количества вещества по первичному и вторичному облаку.');
        } else {
          this.backSpeed = values.backSpeed;
          this.frontSpeed = values.frontSpeed;
          this.flangSpeed = values.flangSpeed;
          this.http.backSpeed = this.backSpeed;
          this.http.frontSpeed = this.frontSpeed;
          this.http.flangSpeed = this.flangSpeed;
          this.showResultForTaskOne = true;
          this.taskOneValuesResult = taskOneValues;
        }
      }, () => {
        this.informer.HandleError('Ошибка получения расчета');
      });
    }, () => {
      this.informer.HandleError('Ошибка получения расчета');
    });
  }

  makeTaskTwo(): void {
    this.spin = true;
    this.latLong = this.map.getLngLat();
    const taskTwoValues = this.taskOneValuesResult;
    taskTwoValues.task = 2;
    taskTwoValues.p0 = this.sfg2.controls.p0.value;
    taskTwoValues.area = this.sfg2.controls.area.value;
    taskTwoValues.time = this.sfg2.controls.time.value;
    this.http.makeRequestForTask(
      taskTwoValues
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по определению количества вещества по первичному и вторичному облаку.');
        } else {
          this.perimeter = values.perimeter;
          this.area = values.area;
          this.http.area = this.area;
          this.showResultForTaskTwo = true;
          this.map.flyToMarker();
          this.map.drawFinalPolygon(values.windDegree, this.area);
        }
      }, () => {
        this.informer.HandleError('Ошибка получения расчета');
      });
    }, () => {
      this.informer.HandleError('Ошибка получения расчета');
    });
  }

  makeTaskThree(): void {
    this.spin = true;
    this.latLong = this.map.getLngLat();
    const taskThreeValues = this.taskOneValuesResult;
    taskThreeValues.task = 3;
    taskThreeValues.fire_kind = this.sfg3.controls.fire_kind.value;
    taskThreeValues.tree_kind = this.sfg3.controls.tree_kind.value;
    this.http.makeRequestForTask(
      taskThreeValues
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по определению количества вещества по первичному и вторичному облаку.');
        } else {
          this.count = values.count;
          this.showResultForTaskThree = true;
        }
      }, () => {
        this.informer.HandleError('Ошибка получения расчета');
      });
    }, () => {
      this.informer.HandleError('Ошибка получения расчета');
    });
  }

  makeTaskFour(): void {
    this.spin = true;
    this.latLong = this.map.getLngLat();
    const taskFourValues = this.taskOneValuesResult;
    taskFourValues.task = 4;
    taskFourValues.tree_kind = this.sfg4.controls.tree_kind.value;
    taskFourValues.nagar_height = this.sfg4.controls.nagar_height.value;
    taskFourValues.tree_diameter = this.sfg4.controls.tree_diameter.value;
    this.http.makeRequestForTask(
      taskFourValues
    ).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.http.makeRequestForUUID(res.uuid, res.responseQueueName).pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.spin = false;
        }),
      ).subscribe(values => {
        if (values === null || values === undefined) {
          this.informer.HandleError('Ошибка получения ответа по определению количества вещества по первичному и вторичному облаку.');
        } else {
          this.damageDegree = values.damageDegree;
          this.property = values.property;
          this.dropOffTrees = values.dropOffTrees;
          this.dropOfReserve = values.dropOfReserve;
          this.showResultForTaskFour = true;
        }
      }, () => {
        this.informer.HandleError('Ошибка получения расчета');
      });
    }, () => {
      this.informer.HandleError('Ошибка получения расчета');
    });
  }

  ngDoCheck(): void {
    // this.perimeterOrAreaValid();
    if (this.map.draw.getAll().features.length !== 0) {
      this.map.clearFires = false;
      this.sfg2.controls.area.setValue(this.map.roundedArea);
      this.sfg2.controls.p0.setValue(this.map.perimeter);
    }
    if (this.map.clearFires === true){
      this.map.roundedArea = 0;
      this.map.perimeter = 0;
      this.sfg2.controls.area.setValue(null);
      this.sfg2.controls.p0.setValue(null);
    }
    if (this.sfg2.controls.time.value < 1){
      this.sfg2.controls.time.setValue(null);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
