import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { InformerService } from '../../services/informer.service';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError, map, retryWhen } from 'rxjs/operators';
import { retryIfError } from '../../services/utils';

export interface FiresTaskValues {
  task: number;
  fire_type?: number;
  fire_class?: number;
  p0?: number;
  area?: number;
  time?: number;
  tree_kind?: string;
  fire_kind?: string;
  nagar_height?: number;
  tree_diameter?: number;
  lat?: number;
  lon?: number;
}

export interface AnswersResponse {
  backSpeed: number;
  flangSpeed: number;
  frontSpeed: number;
  perimeter: number;
  area: number;
  windDegree: number;
  count: number;
  damageDegree: number;
  property: string;
  dropOffTrees: number;
  dropOfReserve: number;
}

export interface ResponseFromServer {
  uuid: string;
  responseQueueName: string;
}

@Injectable({
  providedIn: 'root'
})
export class FiresService {
  area: number;
  backSpeed: number;
  frontSpeed: number;
  flangSpeed: number;
  constructor(
    private http: HttpClient,
    private informer: InformerService
  ) {
  }

  getUnsuitableFires(): Observable<string[]> {
    return this.http.get<HttpResponse<string[]>>(`${environment.basicURL}/specs/fires/unsuitable_fires`, {
      observe: 'response' as 'body'
    }).pipe(
      map((res: HttpResponse<string[]>) => {
        return res.body;
      }),
      catchError(() => {
        this.informer.HandleError<string[]>('Ошибка получения пожаров');
        return of([]);
      })
    );
  }

  getUnsuitableTrees(): Observable<string[]> {
    return this.http.get<HttpResponse<string[]>>(`${environment.basicURL}/specs/fires/unsuitable_trees`, {
      observe: 'response' as 'body'
    }).pipe(
      map((res: HttpResponse<string[]>) => {
        return res.body;
      }),
      catchError(() => {
        this.informer.HandleError<string[]>('Ошибка получения древесины');
        return of([]);
      })
    );
  }

  getDamagedTrees(): Observable<string[]> {
    return this.http.get<HttpResponse<string[]>>(`${environment.basicURL}/specs/fires/damage_trees`, {
      observe: 'response' as 'body'
    }).pipe(
      map((res: HttpResponse<string[]>) => {
        return res.body;
      }),
      catchError(() => {
        this.informer.HandleError<string[]>('Ошибка получения поврежденной древесины');
        return of([]);
      })
    );
  }

  makeRequestForUUID(UUID: string, QUEUE: string): Observable<AnswersResponse> {
    return this.http.get<AnswersResponse>(`${environment.basicURL}/response`, {
      observe: 'body',
      params: {
        uuid: UUID,
        queue: QUEUE
      },
      headers: {
        'X-CORRELATION-ID': UUID
      }
    }).pipe(
      retryWhen(errors => {
        return retryIfError(errors, 422);
      }),
      catchError(this.informer.HandleError<AnswersResponse>('Ошибка получения расчетов'))
    );
  }

  makeRequestForTask(task: FiresTaskValues): Observable<ResponseFromServer> {
    return this.http.post<ResponseFromServer>(
      `${environment.basicURL}/calculate/async/fires`, task,
      {
        observe: 'body'
      }
    ).pipe(
      catchError(this.informer.HandleError<ResponseFromServer>('Ошибка получения расчетов'))
    );
  }
}
