export const environment = {
  production: true,
  mapbox: {
    accessToken: 'pk.eyJ1Ijoic29ubnlpb21taSIsImEiOiJja2h4ejQ0NW4yYmF2MnJwYjZ5cDhxZWcyIn0.QcBHO0sHZA_2eJz_ZKU-UA'
  },
  basicURL: 'https://exfire.space/srv',
};
