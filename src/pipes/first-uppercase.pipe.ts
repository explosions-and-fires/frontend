import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'firstUppercase'
})
export class FirstUppercasePipe implements PipeTransform {
  transform(value: string): string {
      if (value === null) {
        return null;
      }
      const cutStr = value.substr(1, value.length);
      const firstUppercase = value[0].toUpperCase();
      return firstUppercase + cutStr;
  }
}
